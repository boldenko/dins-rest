package org.boldenko.phonebook.web;

import lombok.RequiredArgsConstructor;
import org.boldenko.phonebook.core.User;
import org.boldenko.phonebook.core.UserRepository;
import org.boldenko.phonebook.core.UsernameOccupiedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ApiController {

    private final UserRepository userRepository;

    // получения списка всех пользователей (владельцев телефонных книжек)
    @GetMapping("/users")
    //@ResponseBody
    public ResponseEntity<List<User>> users() {
        return new ResponseEntity<>(userRepository.findAll(), HttpStatus.OK);
    }

    // создания, получения (по id), удаления, редактирования пользователя

    @PostMapping("/users")
    public ResponseEntity<User> createNewUser(@RequestBody User user) {
        try {
            return new ResponseEntity<>(userRepository.create(user.getUsername()).flat(), HttpStatus.OK);
        } catch (UsernameOccupiedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping
    public String getUserById(@RequestParam Long id) {
        return "";
    }

}
