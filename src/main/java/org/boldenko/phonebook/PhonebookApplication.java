package org.boldenko.phonebook;

import org.boldenko.phonebook.core.User;
import org.boldenko.phonebook.core.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class PhonebookApplication {
    public static void main(String[] args) {
        SpringApplication.run(PhonebookApplication.class, args);
    }

    @Bean
    public UserRepository userRepository() {
        return new UserRepository();
    }
}
