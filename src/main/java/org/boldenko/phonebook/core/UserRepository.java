package org.boldenko.phonebook.core;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class UserRepository {

    private Long counter = 0L;
    private final Map<Long, User> users = new HashMap<>();

    public List<User> findAll() {
        return users.values().stream().map(user -> new User(user.getId(), user.getUsername())).collect(Collectors.toList());
    }



    public User create(String username) throws UsernameOccupiedException{

        if (users.values().stream().filter(user -> user.getUsername().equals(username)).count() != 0)
            throw new UsernameOccupiedException();

        User user = new User(counter, username);
        user.setPhonebook(new Phonebook());
        users.put(counter++, user);
        return user;
    }

}
