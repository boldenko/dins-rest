package org.boldenko.phonebook.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {

    public User(Long id, String username) {
        this.id = id;
        this.username = username;
    }

    private Long id;
    private String username;

    private Phonebook phonebook;

    public User flat() {
        return new User(id, username);
    }
}
