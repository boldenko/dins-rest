package org.boldenko.phonebook.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.boldenko.phonebook.core.User;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ApiController.class)
@RunWith(SpringRunner.class)
class ApiControllerTest {


    @Autowired
    private MockMvc mockMvc;

    private final ObjectMapper mapper = new ObjectMapper();

//    @BeforeAll
//    public void populateData(){
//
//    }


    @Test
    public void createUserTest() throws Exception {
        //mockMvc.perform(get("/api/users")).andExpect()

        String bob = "bobdylan";
        createUserAllowed(bob);
        createUserForbidden(bob);

        String hank = "hankhill";
        createUserAllowed(hank);
        createUserForbidden(hank);

        String vasya = "vasiliypoopkin";
        createUserAllowed(vasya);
        createUserForbidden(vasya);

        checkIfAllHere(bob, hank, vasya);




    }

    public void createUserAllowed(String username) throws Exception {
        User user = new User();
        user.setUsername(username);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/users/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(user)))
                .andExpect(status().isOk());
    }

    public void createUserForbidden(String username) throws Exception{
        User user = new User();
        user.setUsername(username);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/users/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(user)))
                .andExpect(status().isForbidden());
    }

    public void checkIfAllHere(String... a) throws Exception {
        MvcResult result = mockMvc.perform(get("/api/users/")).andExpect(status().isOk()).andReturn();
        List<User> usersReturned = mapper.readValue(result.getResponse().getContentAsString(), List.class);


        assertEquals(a.length, usersReturned.size());
        Arrays.stream(a).peek(username -> assertTrue(usersReturned.contains(username)));


    }

}